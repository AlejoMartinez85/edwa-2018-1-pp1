<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of characterFactory
 *
 * @author alejomartinez
 */
class characterFactory implements ICharacterInterface  {
    //put your code here
    
    public static function getWarrior($nombre, $raza, $hp, $mn, $str, $md, $ag): Warrior{
        return new Warrior($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }

    public static function getMage($nombre, $raza, $hp, $mn, $str, $md, $ag): Mage {
        return new JupiterAlien($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }

    public static function getRogue($nombre, $raza, $hp, $mn, $str, $md, $ag): Rogue {
        return new MarsAlien($nombre, $raza, $hp, $mn, $str, $md, $ag);
    }
}
