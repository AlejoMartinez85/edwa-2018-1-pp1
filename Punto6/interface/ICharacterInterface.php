<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alejomartinez
 */
interface ICharacterInterface {
    //put your code here
    public static function getWarrior($nombre, $raza, $hp, $mn, $str, $md, $ag): Warrior;
    public static function getMage($nombre, $raza, $hp, $mn, $str, $md, $ag): Mage;
    public static function getRogue($nombre, $raza, $hp, $mn, $str, $md, $ag): Rogue;
}
